# godbx

A [Go](https://go.dev) library and command line tool for the [Dropbox API](https://www.dropbox.com/developers/documentation/http/overview).

## Installation

To add the library to your module use:

```console
go get gitlab.com/srcgo/godbx
```

To install the command line tool use:

```console
go install gitlab.com/srcgo/godbx/cmd/godbx@latest
```

> For further build options see the provided [build script](./bin/build).
> You may alternatively grab a prebuild binary from the [Releases](https://gitlab.com/srcgo/godbx/-/releases) page.

## Example

Following example authorizes a user, if necessary, and downloads a file from the configured Dropbox account:

```golang
func main() {
    // creates a default client without any security measures
    client := godbx.Default()

    // config must atleast provide `app_key` and `app_secret`
    client.LoadConfig("config.json")

    // check if config provides enough information to request an `access_token`
    if err := client.RequestAccessToken(); err != nil {
        // prompt user to grant access otherwise
        for client.AuthorizeCli() {
            // attempt to request `refresh_token` (and `access_token`)
            if err = client.RequestRefreshToken(); err != nil {
                // in case of an error print it and prompt user again
                fmt.Fprintln(os.Stderr, err.Error())
            } else {
                // save received tokens to config file
                client.SaveConfig("config.json")
                break // stop prompting the user
            }
        }
    }

    // download the file `/foo` from the dropbox account
    // defined by the config and print its content to stdout
    sb := strings.Builder{}
    client.Download("/foo", &sb)
    fmt.Println(sb.String())
}
```

> Error handling in this example is toned down to the absolute necessary.

The **initial** `config.json` might look something like this:

```json
{
    "app_key": "<YOUR_APP_KEY>",
    "app_secret": "<YOUR_APP_SECRET>"
}
```

> You can grab the `app_key` and `app_secret` for your app in the [App Console](https://www.dropbox.com/developers/apps) of your Dropbox.
> The config file may be updated upon receiving responses from requests for access information. It is highly adviced to encrypt the file when it is intended to be stored on a long term.
> You may also want to check out the [source](./cmd/godbx/) of the command line tool as an example of how a config file can be secured.

## CLI Tool

The godbx cli tool provides secure access to the [Dropbox API](https://www.dropbox.com/developers/documentation/http/overview) from a command line. Following the output of `godbx -h`:

```console
Usage of godbx.exe:
  -conf config
        Path to a json config file. (default "./config.json")
  -del delete
        This flag accepts the path to a file or folder and deletes all data at the
        given path in the configured dropbox.
  -down download
        This flag accepts the path to a file, on the configured dropbox, which is
        downloaded and printed to stdout.
  -enc encrypt
        If the -enc flag is provided any config data written to files will be
        encrypted, given either -pass or -sig are provided.
  -pass password
        This flag accepts a password which is used for en- and decryption of the
        config file. May be used in conjunction with the -enc and/or -sig flag.
  -sig signature
        If the -sig flag is provided various information from the current host are used
        to generate a unique signature which is used for en- and decryption of the
        config file. May be used in conjunction with the -enc and/or -pass flag. Note
        that any changes in the environment of the executable (either hardware or
        software) might unexpectedly change the generated signature therefore making any
        previously encrypted config files useless.
  -up upload
        This flag accepts the path to a file, on the configured dropbox. Any data read
        from stdin will be uploaded and stored in that file. May overwrite an existing
        file on the same path.
```

## Packages

This project consists of following packages:

- [godbx](./godbx/)
- [cmd/godbx](./cmd/godbx/)
- [cmd/godbx/flags](./cmd/godbx/flags/)

## License

See [LICENSE](./LICENSE) file.
