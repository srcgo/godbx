package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/host"
	"github.com/shirou/gopsutil/v3/mem"
	"github.com/shirou/gopsutil/v3/process"
)

// some random value (see 'bin/seed -h' for generation)
const client_seed = 111111111111111111

func init() {
	tryOrWarn(cpu.Percent(0, false))
}

func hash32(data string) [32]byte {
	return sha256.Sum256([]byte(data))
}

func encrypt(data []byte, key [32]byte) []byte {
	rand := rand.New(rand.NewSource(mkseed()))
	ciph := tryOrErr(aes.NewCipher(key[:]))
	gcm := tryOrErr(cipher.NewGCM(ciph))
	nonce := make([]byte, gcm.NonceSize())
	tryOrErr(rand.Read(nonce))

	enc, _ := checksum(gcm.Seal(nonce, nonce, data, nil), true)
	return enc
}

func decrypt(data []byte, key [32]byte) ([]byte, error) {
	data, hadChecksum := checksum(data, false)

	if !hadChecksum {
		return data, nil // data was not encrypted
	}

	ciph := tryOrErr(aes.NewCipher(key[:]))
	gcm := tryOrErr(cipher.NewGCM(ciph))
	nonceSize := gcm.NonceSize()

	if len(data) < nonceSize {
		return data, errors.New("decryption failed, missing data")
	}

	data_backup := data
	nonce, data := data[:nonceSize], data[nonceSize:]
	plain, err := gcm.Open(nil, nonce, data, nil)

	if err != nil {
		return data_backup, errors.New("decryption failed, invalid key or data corrupted")
	}

	return plain, nil
}

// Adds or removes a client based checksum to mark encrypted data.
// Returns the data with the checksum added or removed and wether
// changes took place.
func checksum(data []byte, add bool) ([]byte, bool) {
	clientSignature := func() []byte {
		s := fmt.Sprintf("%-64d", client_seed)
		r := hash32(s)
		return r[:]
	}

	if add {
		csum := clientSignature()
		return append(csum[:], data...), true
	}

	if len(data) >= 32 {
		csum, rawdata := data[:32], data[32:]

		if bytes.Equal(csum, clientSignature()) {
			return rawdata, true
		}
	}

	return data, false
}

// Generates a 32-byte long unique signature based on host
// information, that is unlikely to change, and the given password.
func mksig(pass string) [32]byte {
	key := strings.Builder{}
	key.WriteString(pass)

	vmem := tryOrWarn(mem.VirtualMemory())
	hostInfo := tryOrWarn(host.Info())
	cpuInfos := tryOrWarn(cpu.Info())
	diskParts := tryOrWarn(disk.Partitions(false))
	key.WriteString(fmt.Sprintf("%v%v", hostInfo.HostID, vmem.Total))

	for _, cpuInfo := range cpuInfos {
		key.WriteString(fmt.Sprintf("%v%v", cpuInfo.PhysicalID, cpuInfo.VendorID))
	}

	for _, diskPart := range diskParts {
		diskUsage := tryOrWarn(disk.Usage(diskPart.Device))
		key.WriteString(fmt.Sprintf("%v", diskUsage.Total))
	}

	return sha256.Sum256([]byte(key.String()))
}

// Generates a random seed based on host information that is known
// to change frequently.
func mkseed() int64 {
	key := strings.Builder{}
	vmem := tryOrWarn(mem.VirtualMemory())
	hostInfo := tryOrWarn(host.Info())
	cpuPerc := tryOrWarn(cpu.Percent(0, false))
	diskParts := tryOrWarn(disk.Partitions(false))
	pids := tryOrWarn(process.Pids())

	key.WriteString(fmt.Sprintf("%v%v%v%v%v",
		vmem.Used, cpuPerc, hostInfo.Uptime, pids, time.Now().UnixNano()))

	for _, diskPart := range diskParts {
		diskUsage := tryOrWarn(disk.Usage(diskPart.Device))
		key.WriteString(fmt.Sprintf("%v", diskUsage.Used))
	}

	csum := sha256.Sum256([]byte(key.String()))
	return int64(binary.BigEndian.Uint64(csum[12:20]))
}
