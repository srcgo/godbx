# flags

## Constants

```golang
const Config = "conf"
```

```golang
const Delete = "del"
```

```golang
const Download = "down"
```

```golang
const Encrypt = "enc"
```

```golang
const Password = "pass"
```

```golang
const Signature = "sig"
```

```golang
const Upload = "up"
```

## Variables

Default values of available flags.

```golang
var Defaults = map[string]interface{}{
    Config:    "./config.json",
    Encrypt:   false,
    Signature: false,
    Password:  "",
    Download:  "",
    Upload:    "",
    Delete:    "",
}
```

Descriptions of available flags.

```golang
var Descriptions = map[string]string{
    Config:    wrap(fmt.Sprintf(confDescription), wrapAt),
    Encrypt:   wrap(fmt.Sprintf(encDescription, Encrypt, Password, Signature), wrapAt),
    Signature: wrap(fmt.Sprintf(sigDescription, Signature, Encrypt, Password), wrapAt),
    Password:  wrap(fmt.Sprintf(passDescription, Encrypt, Signature), wrapAt),
    Download:  wrap(fmt.Sprintf(downDescription), wrapAt),
    Upload:    wrap(fmt.Sprintf(upDescription), wrapAt),
    Delete:    wrap(fmt.Sprintf(delDescription), wrapAt),
}
```
