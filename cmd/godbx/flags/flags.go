package flags

import (
	"fmt"
	"strings"
)

const Config = "conf"
const confDescription = "Path to a json `config` file."

const Encrypt = "enc"
const encDescription = "If the -%s flag is provided any config " +
	"data written to files will be `encrypt`ed, given either -%s or -%s " +
	"are provided."

const Signature = "sig"
const sigDescription = "If the -%s flag is provided various " +
	"information from the current host are used to generate a unique " +
	"`signature` which is used for en- and decryption of the config " +
	"file. May be used in conjunction with the -%s and/or -%s flag. " +
	"Note that any changes in the environment of the executable " +
	"(either hardware or software) might unexpectedly change the " +
	"generated signature therefore making any previously encrypted " +
	"config files useless."

const Password = "pass"
const passDescription = "This flag accepts a `password` which is " +
	"used for en- and decryption of the config file. May be used in " +
	"conjunction with the -%s and/or -%s flag."

const Download = "down"
const downDescription = "This flag accepts the path to a file, on the " +
	"configured dropbox, which is `download`ed and printed to stdout."

const Upload = "up"
const upDescription = "This flag accepts the path to a file, on the " +
	"configured dropbox. Any data read from stdin will be `upload`ed and " +
	"stored in that file. May overwrite an existing file on the same path."

const Delete = "del"
const delDescription = "This flag accepts the path to a file or folder " +
	"and `delete`s all data at the given path in the configured dropbox."

const wrapAt = 80

// Descriptions of available flags.
var Descriptions = map[string]string{
	Config:    wrap(fmt.Sprintf(confDescription), wrapAt),
	Encrypt:   wrap(fmt.Sprintf(encDescription, Encrypt, Password, Signature), wrapAt),
	Signature: wrap(fmt.Sprintf(sigDescription, Signature, Encrypt, Password), wrapAt),
	Password:  wrap(fmt.Sprintf(passDescription, Encrypt, Signature), wrapAt),
	Download:  wrap(fmt.Sprintf(downDescription), wrapAt),
	Upload:    wrap(fmt.Sprintf(upDescription), wrapAt),
	Delete:    wrap(fmt.Sprintf(delDescription), wrapAt),
}

// Default values of available flags.
var Defaults = map[string]interface{}{
	Config:    "./config.json",
	Encrypt:   false,
	Signature: false,
	Password:  "",
	Download:  "",
	Upload:    "",
	Delete:    "",
}

// Wraps the text at the given lineLen. Replaces existing line
// endings with spaces.
func wrap(text string, lineLen int) string {
	words := strings.Split(strings.ReplaceAll(text, "\n", " "), " ")
	wrapped := strings.Builder{}
	line := strings.Builder{}

	for _, word := range words {
		if line.Len()+len(word)+1 > lineLen {
			wrapped.WriteString(line.String() + "\n")
			line.Reset()
		}

		if line.Len() > 0 {
			line.WriteRune(' ')
		}

		line.WriteString(word)
	}

	wrapped.WriteString(line.String())
	return wrapped.String()
}
