package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"strings"

	"gitlab.com/srcgo/godbx/cmd/godbx/flags"
	"gitlab.com/srcgo/godbx/godbx"
)

func main() {
	// for the looks
	os.Args[0] = path.Base(strings.ReplaceAll(os.Args[0], "\\", "/"))

	// parse command line arguments
	var configFile, up, down, del string
	var configIn func([]byte) []byte
	var configOut func([]byte) []byte
	parseFlags(&configFile, &configIn, &configOut, &down, &up, &del)

	// initialize client and load config
	client := godbx.New(configIn, configOut)
	reportError(client.LoadConfig(configFile))

	// attempt to request access_token
	if err := client.RequestAccessToken(); err != nil {
		reportWarning(err)

		// prompt user to grant access
		for client.AuthorizeCli() {
			// attempt to request refresh_token
			if err = client.RequestRefreshToken(); err != nil {
				reportWarning(err)
			} else {
				reportError(client.SaveConfig(configFile))
				break
			}
		}
	} else {
		reportError(client.SaveConfig(configFile))
	}

	if len(up) > 0 {
		reportError(client.Upload(os.Stdin, up))
	}

	if len(down) > 0 {
		sb := strings.Builder{}
		reportError(client.Download(down, &sb))
		fmt.Print(sb.String())
	}

	if len(del) > 0 {
		reportError(client.Delete(del))
	}

	os.Exit(0)
}

func parseFlags(configFile *string, configIn *func([]byte) []byte, configOut *func([]byte) []byte, down *string, up *string, del *string) {
	flag.StringVar(configFile, flags.Config, flags.Defaults[flags.Config].(string), flags.Descriptions[flags.Config])
	flag.StringVar(down, flags.Download, flags.Defaults[flags.Download].(string), flags.Descriptions[flags.Download])
	flag.StringVar(up, flags.Upload, flags.Defaults[flags.Upload].(string), flags.Descriptions[flags.Upload])
	flag.StringVar(del, flags.Delete, flags.Defaults[flags.Delete].(string), flags.Descriptions[flags.Delete])
	pass := flag.String(flags.Password, flags.Defaults[flags.Password].(string), flags.Descriptions[flags.Password])
	sig := flag.Bool(flags.Signature, flags.Defaults[flags.Signature].(bool), flags.Descriptions[flags.Signature])
	enc := flag.Bool(flags.Encrypt, flags.Defaults[flags.Encrypt].(bool), flags.Descriptions[flags.Encrypt])
	flag.Parse()

	if *sig || len(*pass) > 0 {
		var hash [32]byte

		if !*sig {
			hash = hash32(*pass)
		} else {
			hash = mksig(*pass)
		}

		*configIn = func(data []byte) []byte { return tryOrErr(decrypt(data, hash)) }

		if *enc {
			*configOut = func(data []byte) []byte { return encrypt(data, hash) }
		} else {
			*configOut = func(data []byte) []byte { return data }
		}
	} else {
		*configIn = func(data []byte) []byte { return data }
		*configOut = func(data []byte) []byte { return data }
	}
}

func reportWarning(err error) {
	if err != nil {
		printError(err, "warn")
	}
}

func reportError(err error) {
	if err != nil {
		printError(err, "errr")
		os.Exit(1)
	}
}

// Panics if err is nil.
func printError(err error, desc string) {
	fmt.Fprintf(os.Stderr,
		"%s:%s: %s\n",
		os.Args[0], desc, err.Error())
}

func tryOrWarn[T any](result T, err error) T {
	reportWarning(err)
	return result
}

func tryOrErr[T any](result T, err error) T {
	reportError(err)
	return result
}
