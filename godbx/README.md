# godbx

## Constants

```golang
const AuthEndpoint = "https://www.dropbox.com/oauth2/authorize"
```

```golang
const FilesEndpoint = "https://content.dropboxapi.com/2/files"
```

```golang
const TokenEndpoint = "https://api.dropboxapi.com/oauth2/token"
```

## Types

### type [Client](./client.go#L13)

`type Client struct { ... }`

Client provides an access point for authorization,
authentification and interaction with the dropbox api.

#### func [Default](./client.go#L43)

`func Default() *Client`

Default creates a Client with configIn- and OutPipe without any
effect and an empty config.

#### func [New](./client.go#L31)

`func New(configInPipe func([]byte) []byte, configOutPipe func([]byte) []byte) *Client`

New creates a Client with the given configIn- and OutPipe and an
empty config. Any data read from or written to a config file, by
this client, is piped through the pipes. This feature is intended
to be used for security (i.e. en- and decryption of the config
file). Checkout the [godbx cli tool] for an example.

[godbx cli tool]: [https://gitlab.com/srcgo/godbx/cmd/](https://gitlab.com/srcgo/godbx/cmd/)

#### func (*Client) [AuthorizeCli](./auth.go#L20)

`func (client *Client) AuthorizeCli() bool`

AuthorizeCli prompts the user to authorize the app and enter the
provided 'app_code'. Updates the corresponding field in the
config on success. The config must contain a valid 'app_key'
otherwise the prompted url will be malformed. Returns false if
the user cancels the input. This function may be called if
[RequestRefreshToken] fails.

#### func (*Client) [Delete](./api_files.go#L29)

`func (client *Client) Delete(path string) error`

Delete deletes the file or folder at the given path, in the
configured dropbox.

#### func (*Client) [Download](./api_files.go#L21)

`func (client *Client) Download(path string, wr io.Writer) error`

Download downloads all data from the file at the given path, in
the configured dropbox, and writes it to wr.

#### func (*Client) [LoadConfig](./client.go#L71)

`func (client *Client) LoadConfig(file string) error`

LoadConfig loads a client config from the given file and assigns
it to the client.

#### func (*Client) [RequestAccessToken](./api_token.go#L15)

`func (client *Client) RequestAccessToken() error`

RequestAccessToken attempts to request a new 'access_token' with
the current client config and update the config on success.
Returns an error if the config does not provide the information
required for authentification. This function may be called if
other API requests fail.

#### func (*Client) [RequestRefreshToken](./api_token.go#L34)

`func (client *Client) RequestRefreshToken() error`

RequestRefreshToken attempts to request a new 'refresh_token'
with the current client config and update the config on success.
Returns an error if the config does not provide the information
required for authentification. This function may be called if
[RequestAccessToken] fails.

#### func (*Client) [SaveConfig](./client.go#L59)

`func (client *Client) SaveConfig(file string) error`

SaveConfig saves the current client config to the given file.

#### func (*Client) [SetConfig](./client.go#L54)

`func (client *Client) SetConfig(config map[string]interface{ ... })`

SetConfig assigns the given config to the client.

#### func (*Client) [Upload](./api_files.go#L13)

`func (client *Client) Upload(rd io.Reader, path string) error`

Upload uploads all data read from rd to a file at the given path,
in the configured dropbox.
