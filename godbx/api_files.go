package godbx

import (
	"encoding/json"
	"io"
	"strings"
)

const FilesEndpoint = "https://content.dropboxapi.com/2/files"

// Upload uploads all data read from rd to a file at the given path,
// in the configured dropbox.
func (client *Client) Upload(rd io.Reader, path string) error {
	return client.doPost(
		FilesEndpoint+"/upload",
		map[string]string{"path": path, "mode": "overwrite"}, rd, nil, false)
}

// Download downloads all data from the file at the given path, in
// the configured dropbox, and writes it to wr.
func (client *Client) Download(path string, wr io.Writer) error {
	return client.doPost(
		FilesEndpoint+"/download",
		map[string]string{"path": path}, nil, wr, false)
}

// Delete deletes the file or folder at the given path, in the
// configured dropbox.
func (client *Client) Delete(path string) error {
	jsArgs, _ := json.Marshal(map[string]string{"path": path})

	return client.doPost(
		"https://api.dropboxapi.com/2/files/delete_v2", // why a different url dropbox? -.-
		nil, strings.NewReader(string(jsArgs)), nil, false)
}
