package godbx

import (
	"fmt"
	"strings"
)

const TokenEndpoint = "https://api.dropboxapi.com/oauth2/token"

// RequestAccessToken attempts to request a new 'access_token' with
// the current client config and update the config on success.
// Returns an error if the config does not provide the information
// required for authentification. This function may be called if
// other API requests fail.
func (client *Client) RequestAccessToken() error {
	sb := strings.Builder{}

	err := client.doPost(TokenEndpoint, nil, strings.NewReader(fmt.Sprintf(
		"grant_type=refresh_token&refresh_token=%s",
		client.config["refresh_token"])), &sb, true)

	if err == nil {
		err = client.updateConfig(&sb)
	}

	return err
}

// RequestRefreshToken attempts to request a new 'refresh_token'
// with the current client config and update the config on success.
// Returns an error if the config does not provide the information
// required for authentification. This function may be called if
// [RequestAccessToken] fails.
func (client *Client) RequestRefreshToken() error {
	sb := strings.Builder{}

	err := client.doPost(TokenEndpoint, nil, strings.NewReader(fmt.Sprintf(
		"code=%s&grant_type=authorization_code",
		client.config["app_code"])), &sb, true)

	if err == nil {
		err = client.updateConfig(&sb)
	}

	return err
}
