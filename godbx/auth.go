package godbx

import (
	"bufio"
	"fmt"
	"os"
)

const AuthEndpoint = "https://www.dropbox.com/oauth2/authorize"

const promptMessage = "Please visit %s and authorize the app. Copy the " +
	"provided app code into the console and press enter to continue: "

// AuthorizeCli prompts the user to authorize the app and enter the
// provided 'app_code'. Updates the corresponding field in the
// config on success. The config must contain a valid 'app_key'
// otherwise the prompted url will be malformed. Returns false if
// the user cancels the input. This function may be called if
// [RequestRefreshToken] fails.
func (client *Client) AuthorizeCli() bool {
	reqUrl := fmt.Sprintf(
		"%s?client_id=%s&token_access_type=offline&response_type=code",
		AuthEndpoint, client.config["app_key"])

	// prompt user to grant access
	fmt.Printf(promptMessage, reqUrl)

	if sc := bufio.NewScanner(os.Stdin); sc.Scan() {
		// update app_code in client config
		client.config["app_code"] = sc.Text()
		return true
	}

	return false
}
