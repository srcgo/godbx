package godbx

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// Client provides an access point for authorization,
// authentification and interaction with the dropbox api.
type Client struct {
	// configOutPipe is invoked for any config data written to a config file.
	configOutPipe func(data []byte) []byte

	// configInPipe is invoked for any config data read from a config file.
	configInPipe func(data []byte) []byte

	// config contains all Client information.
	config map[string]interface{}
}

// New creates a Client with the given configIn- and OutPipe and an
// empty config. Any data read from or written to a config file, by
// this client, is piped through the pipes. This feature is intended
// to be used for security (i.e. en- and decryption of the config
// file). Checkout the [godbx cli tool] for an example.
//
// [godbx cli tool]: https://gitlab.com/srcgo/godbx/cmd/
func New(configInPipe func([]byte) []byte, configOutPipe func([]byte) []byte) *Client {
	client := Client{
		configOutPipe: configOutPipe,
		configInPipe:  configInPipe,
		config:        map[string]interface{}{},
	}

	return &client
}

// Default creates a Client with configIn- and OutPipe without any
// effect and an empty config.
func Default() *Client {
	client := Client{
		configOutPipe: func(data []byte) []byte { return data },
		configInPipe:  func(data []byte) []byte { return data },
		config:        map[string]interface{}{},
	}

	return &client
}

// SetConfig assigns the given config to the client.
func (client *Client) SetConfig(config map[string]interface{}) {
	client.config = config
}

// SaveConfig saves the current client config to the given file.
func (client *Client) SaveConfig(file string) error {
	data, err := json.MarshalIndent(client.config, "", "\t")

	if err == nil {
		err = ioutil.WriteFile(file, client.configOutPipe(data), 0666)
	}

	return err
}

// LoadConfig loads a client config from the given file and assigns
// it to the client.
func (client *Client) LoadConfig(file string) error {
	fl, err := os.Open(file)

	if err == nil {
		defer fl.Close()
		var data []byte

		data, err = io.ReadAll(fl)

		if err == nil {
			err = json.Unmarshal(client.configInPipe(data), &client.config)
		}
	}

	return err
}

// Updates the fields in the client config provided by s. Returns an
// error if the string created from s is not in json format.
func (client *Client) updateConfig(s fmt.Stringer) error {
	var m map[string]interface{}
	err := json.Unmarshal([]byte(s.String()), &m)

	if err == nil {
		for k, v := range m {
			client.config[k] = v
		}
	}

	return err
}
