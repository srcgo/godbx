package godbx

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

// Sends a post request to the given url.
//   - reqUrl: URL to send the request to. If auth is set to true must use the https protocol.
//   - apiArgs: Arguments provided to dropbox api request. May be nil.
//   - rd: Data for the request body is read from the reader. May be nil.
//   - wr: Data from the response body is written to the writer. May be nil.
//   - auth: Wether an authorization header should be added. If set to true reqUrl must use the https protocol.
func (client *Client) doPost(reqUrl string, apiArgs map[string]string, rd io.Reader, wr io.Writer, auth bool) error {
	req, err := http.NewRequest("POST", reqUrl, rd)

	if err == nil {
		if auth {
			if strings.HasPrefix(reqUrl, "https://") {
				req.SetBasicAuth(
					client.config["app_key"].(string),
					client.config["app_secret"].(string))
			} else {
				err = fmt.Errorf("authorization requests require the https protocol")
			}
		} else {
			var jsArgs []byte
			jsArgs, err = json.Marshal(apiArgs)

			if err == nil {
				req.Header = map[string][]string{
					"Authorization": {fmt.Sprintf("Bearer %s", client.config["access_token"])},
				}

				if apiArgs != nil {
					req.Header["Dropbox-API-Arg"] = []string{string(jsArgs)}
					req.Header["Content-Type"] = []string{"application/octet-stream"}
				} else {
					req.Header["Content-Type"] = []string{"application/json"} // todo contentType param?
				}
			}
		}

		if err == nil {
			var res *http.Response
			httpClient := http.Client{}
			res, err = httpClient.Do(req)

			if err == nil {
				defer res.Body.Close()
				err = checkResponse(res)

				if err == nil {
					if wr != nil {
						var resBody []byte
						resBody, err = io.ReadAll(res.Body)

						if err == nil {
							_, err = wr.Write(resBody)
						}
					}
				}
			}
		}
	}

	return err
}

// Checks the http response and returns an error, containing
// information of the respnse body, if it does not match certain
// criteria (e.g. the status code beeing something different than
// 2XX).
func checkResponse(res *http.Response) error {
	if res.StatusCode < 200 || res.StatusCode > 299 {
		resBody, err := ioutil.ReadAll(res.Body)

		if err == nil {
			var resMap map[string]string
			err = json.Unmarshal(resBody, &resMap)

			if err == nil {
				err = fmt.Errorf("%s, %s",
					resMap["error"],
					resMap["error_description"])
			} else {
				err = fmt.Errorf(string(resBody))
			}
		}

		return err
	}

	return nil
}
